$(document).ready(function(){
    $('.top-icon').click(function () {
        $(this).toggleClass('top-icon-click');
        $('.popup-nav-menu').slideToggle('hide');
    });
    $('.btn-show-block').click(function (){
        $('.top-rated-hot-news').slideToggle('hide');
    });

    $('.nav-menu-li').click(function () {
        let anchorLink = $(this).attr('href');
        let destination = $(anchorLink).offset().top;
        $('html, body').animate({
            scrollTop: destination,
        }, 1000);
    });


});


$(document).scroll(function () {
    const $screenHeight = 550;
    const $currentPosition = $(window).scrollTop();


    if ($currentPosition > $screenHeight){
        if (!$('.scroll-top-to-btn').length){
            const $scrollTopBtn = $('<button  class="scroll-top-to-btn">BACK TO THE TOP</button>');
            $('.top-rated-hot-news').before($scrollTopBtn);
            $scrollTopBtn.click(()=>{
                $('body , html').animate({
                    scrollTop: 0,
                },2000);
            })
        }
    }else {

        $('.scroll-top-to-btn').fadeOut(1500,()=>{
            $('.scroll-top-to-btn').remove();

        })
    }
});